# Information / Информация

SPEC-файл для создания RPM-пакета **rclone**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/rclone`.
2. Установить пакет: `dnf install rclone`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)