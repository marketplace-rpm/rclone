%global d_bin                   %{_bindir}
%global d_man                   %{_mandir}

Name:                           rclone
Version:                        1.50.0
Release:                        100%{?dist}
Summary:                        Rsync for cloud storage
License:                        MIT
URL:                            https://rclone.org/

Source10:                       rclone
Source11:                       rclone.1


%description
Rclone is a command line program to sync files and directories to and
from various cloud services.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%prep


%install
%{__rm} -rf %{buildroot}

%{__install} -Dp -m 0755 %{SOURCE10} \
  %{buildroot}%{d_bin}/rclone

%{__install} -Dp -m 0644 %{SOURCE11} \
  %{buildroot}%{d_man}/man1/rclone.1


%files
%{d_bin}/rclone
%{d_man}/man1/rclone.1*


%changelog
* Wed Oct 30 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.50.0-100
- Initial build.
